<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-01-31
 * Time: 18:04:15
 * Info:
 */

class auth_group_model
{

    public static function getRoleGroup()
    {
        $list = D('auth_group')->field("id,name")->where(['status' => 'normal'])->select();

        return $list;
    }

}
