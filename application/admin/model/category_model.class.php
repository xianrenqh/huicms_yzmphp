<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-02-14
 * Time: 16:31:30
 * Info:
 */
yzm_base::load_sys_class('auth', '', 0);

class category_model
{

    public static function getPidMenuList()
    {
        $list        = D("category")->field('id,parent_id,name')->where(['delete_time' => 0, 'status' => 1])->select();
        $pidMenuList = self::buildPidMenu(0, $list);

        return $pidMenuList;
    }

    public static function buildPidMenu($pid, $list, $level = 0)
    {
        $newList = [];
        foreach ($list as $vo) {
            if ($vo['parent_id'] == $pid) {
                $level++;
                foreach ($newList as $v) {
                    if ($vo['parent_id'] == $v['parent_id'] && isset($v['level'])) {
                        $level = $v['level'];
                        break;
                    }
                }
                $vo['level'] = $level;
                if ($level > 1) {
                    $repeatString = "----";
                    $markString   = str_repeat("│{$repeatString}", $level - 1);
                    $vo['name']  = $markString.$vo['name'];
                }
                $newList[] = $vo;
                $childList = self::buildPidMenu($vo['id'], $list, $level);
                ! empty($childList) && $newList = array_merge($newList, $childList);
            }

        }

        return $newList;
    }
}
