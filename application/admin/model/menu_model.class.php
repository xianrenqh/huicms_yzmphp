<?php
yzm_base::load_sys_class('auth', '', 0);

class menu_model
{

    /**
     * 获取后台菜单树信息
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getMenuTree()
    {
        $authServer = new auth();

        return $this->buildMenuChild(0, $this->getMenuData(), $authServer);
    }

    /**
     * 获取所有菜单数据
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function getMenuData()
    {
        $menuData = D('auth_rule')->field('id,pid,title,icon,node,type')->where("delete_time",
            0)->where("is_menu=1")->order("sort asc,id asc")->select();
        foreach ($menuData as $key => $v) {
            $menuData[$key]['href'] = $v['node'];
        }

        return $menuData;
    }

    private function buildMenuChild($pid, $menuList, $authServer)
    {
        $uid      = cmf_get_admin_id();
        $treeList = [];
        foreach ($menuList as &$v) {
            $check = empty($v['node']) ? true : $authServer->check($v['node'], $uid);
            if ($pid == $v['pid'] && $check) {
                $node = $v;

                $child = $this->buildMenuChild($v['id'], $menuList, $authServer);
                if ( ! empty($child)) {
                    $node['children'] = $child;
                }
                if ( ! empty($v['node']) || ! empty($child)) {
                    $treeList[] = $node;
                }
            }
        }

        return $treeList;
    }

    public static function getPidMenuList()
    {
        $list        = D("auth_rule")->field('id,pid,title')->where(['delete_time' => 0])->select();
        $pidMenuList = self::buildPidMenu(0, $list);
        $pidMenuList = array_merge([
            [
                'id'    => 0,
                'pid'   => 0,
                'title' => '顶级菜单',
            ]
        ], $pidMenuList);

        return $pidMenuList;
    }

    public static function buildPidMenu($pid, $list, $level = 0)
    {
        $newList = [];
        foreach ($list as $vo) {
            if ($vo['pid'] == $pid) {
                $level++;
                foreach ($newList as $v) {
                    if ($vo['pid'] == $v['pid'] && isset($v['level'])) {
                        $level = $v['level'];
                        break;
                    }
                }
                $vo['level'] = $level;
                if ($level > 1) {
                    $repeatString = "----";
                    $markString   = str_repeat("│{$repeatString}", $level - 1);
                    $vo['title']  = $markString.$vo['title'];
                }
                $newList[] = $vo;
                $childList = self::buildPidMenu($vo['id'], $list, $level);
                ! empty($childList) && $newList = array_merge($newList, $childList);
            }

        }

        return $newList;
    }

}
