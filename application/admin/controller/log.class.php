<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-03-30
 * Time: 14:36:47
 * Info: 后台日志控制器
 */
defined('IN_YZMPHP') or exit('Access Denied');
yzm_base::load_controller('common', 'admin', 0);

class log extends common
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 登录日志列表
     * @return void
     */
    public function loginLogList()
    {
        if (is_ajax()) {
            $limit = (int)input('limit', 10);
            $page  = (int)input('page', 1);
            $first = ($page - 1) * $limit;
            $field = input('field', 'id');
            $order = input('order', 'asc');
            $key   = input('key');

            $where = "1=1";
            if ( ! empty($key['user_name'])) {
                $where .= " and username = '{$key['user_name']}'";
            }
            if (isset($key['status']) && $key['status'] != '') {
                $where .= " and login_result = '{$key['status']}'";
            }

            $count = D('admin_login_log')->where($where)->total();
            $list  = D('admin_login_log')->where($where)->limit("{$first},{$limit}")->order("{$field} {$order}")->select();
            foreach ($list as $key => $v) {
                $list[$key]['login_time'] = date('Y-m-d H:i:s', $v['login_time']);
            }
            $this->success('ok', $list, $count);
        }
        include $this->admin_tpl('log/login_log');
    }

    /**
     * 删除登录日志
     */
    public function loginLogDel()
    {
        $type = input('type');
        if ($type == 1) {
            //删除全部
            D('admin_login_log')->delete(['1' => 1]);
        } else {
            $lastTime = time() - 3600 * 24 * 30;
            D('admin_login_log')->delete(['create_time<' => $lastTime]);
        }
        $this->success('删除成功');
    }

    /**
     * 操作日志列表
     * @return void
     */
    public function manageLogList()
    {
        if (is_ajax()) {
            $limit = (int)input('limit', 10);
            $page  = (int)input('page', 1);
            $first = ($page - 1) * $limit;
            $field = input('field', 'id');
            $order = input('order', 'asc');

            $count = D('admin_log')->total();
            $list  = D('admin_log')->limit("{$first},{$limit}")->order("{$field} {$order}")->select();
            foreach ($list as $key => $v) {
                $list[$key]['log_time']    = date('Y-m-d H:i:s', $v['log_time']);
                $list[$key]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->success('ok', $list, $count);
        }
        include $this->admin_tpl('log/manage_log');
    }

    /**
     * 删除登录日志
     */
    public function manageLogDel()
    {
        $type = input('type');
        if ($type == 1) {
            //删除全部
            D('admin_log')->delete(['1' => 1]);
        } else {
            $lastTime = time() - 3600 * 24 * 30;
            D('admin_log')->delete(['create_time<' => $lastTime]);
        }
        $this->success('删除成功');
    }

}
