<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-02-02
 * Time: 11:33:00
 * Info:
 */
defined('IN_YZMPHP') or exit('Access Denied');
yzm_base::load_controller('common', 'admin', 0);

class config extends common
{

    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = D('config');
    }

    public function init()
    {
        $data                 = $this->model->select();
        $data                 = array_column($data, 'value', 'name');
        $data['ueditor_icon'] = array_filter(explode(',', $data['ueditor_icon']));

        $ueditor_icon = config('ueditor_icon');
        $upload_modes = [];

        include $this->admin_tpl('config/index');
    }

    public function save()
    {
        if (is_post()) {
            $param = input('post.');
            if ( ! empty($param['ueditor_icon'])) {
                $param['ueditor_icon'] = implode(',', $param['ueditor_icon']);
            } else {
                $param['ueditor_icon'] = '';
            }
            foreach ($param as $key => $value) {
                $arr[$key] = $value;
                $value     = htmlspecialchars($value);
                $this->model->where(['name' => $key])->update(['value' => $value]);
            }
            delcache('cacheSystemConfig');
            return_success('保存成功');
        }
    }

    /**
     * 自定义配置
     */
    public function customConfig()
    {
        $data = D('config')->where(['type' => 99])->select();
        include $this->admin_tpl('config/custom_config');
    }

    /**
     * 添加自定义配置
     */
    public function custom_config_add()
    {
        if (is_post()) {
            $param = input('post.');
            $cha   = D('config')->where(['name' => $param['name']])->find();
            if ( ! empty($cha)) {
                return_error('配置名称已存在，请修改');
            }
            if ($param['fieldtype'] == "radio" || $param['fieldtype'] == "select") {
                $setting = json_encode(explode('|', rtrim($param['setting'][$param['fieldtype']], '|')));
                $setting = str_replace("：", ":", $setting);
            } else {
                $setting = "";
            }
            $data = [
                'name'      => $param['name'],
                'fieldtype' => $param['fieldtype'],
                'type'      => 99,
                'title'     => $param['title'],
                'status'    => 1,
                'value'     => $param['value'][$param['fieldtype']],
                'setting'   => $setting
            ];
            try {
                D('config')->insert($data);
            } catch (\Exception $e) {
                return_error('操作失败<br>'.$e->getMessage());
            }
            //清除缓存
            delcache('cacheSystemConfig');

            return_success('操作成功');
        }
        include $this->admin_tpl('config/custom_config_add');
    }

    /**
     * 编辑自定义配置
     */
    public function custom_config_edit()
    {
        if (is_post()) {
            $param = input('post.');
            $cha   = D('config')->where(['name' => $param['name'], 'id<>' => $param['id']])->find();
            if ( ! empty($cha)) {
                return_error('配置名称已存在，请修改');
            }
            $data = [
                'name'   => $param['name'],
                'type'   => 99,
                'title'  => $param['title'],
                'status' => $param['status'],
                'value'  => $param['value'][$param['fieldtype']]
            ];
            try {
                $a = D('config')->update($data, ['id' => $param['id']]);
            } catch (\Exception $e) {
                return_error('操作失败<br>'.$e->getMessage());
            }
            //清除缓存
            delcache('cacheSystemConfig');
            return_success('操作成功');
        }
        $id   = input('get.id');
        $data = D('config')->where(['id' => $id])->find();

        $setting_data = '';
        if (isset($data['fieldtype']) && $data['fieldtype'] == 'radio') {
            $setting = json_decode($data['setting'], true);

            foreach ($setting as $k => $v) {
                if (strstr($v, ':')) {
                    $v2           = $v;
                    $v            = explode(":", $v2)[0];
                    $k            = explode(":", $v2)[1];
                    $checked      = $data['value'] == $k ? "checked" : '';
                    $setting_data .= '<input type="radio" name="value[radio]" value="'.$k.'" title="'.$v.'" '.$checked.'>';
                } else {
                    $checked      = $data['value'] == $v ? "checked" : '';
                    $setting_data .= '<input type="radio" name="value[radio]" value="'.$v.'" title="'.$v.'" '.$checked.'>';
                }
            }
        }
        if (isset($data['fieldtype']) && $data['fieldtype'] == 'select') {
            $setting      = json_decode($data['setting'], true);
            $setting_data = "";
            foreach ($setting as $k => $v) {
                if (strstr($v, ':')) {
                    $v2           = $v;
                    $v            = explode(":", $v2)[0];
                    $k            = explode(":", $v2)[1];
                    $selected     = $data['value'] == $k ? "selected" : '';
                    $setting_data .= '<option value="'.$k.'" '.$selected.'>'.$v.'</option>';
                } else {
                    $selected     = $data['value'] == $v ? "selected" : '';
                    $setting_data .= '<option value="'.$v.'" '.$selected.'>'.$v.'</option>';
                }
            }
        }
        include $this->admin_tpl('config/custom_config_edit');
    }

    /**
     * 删除自定义配置
     */
    public function custom_config_delete()
    {
        if (is_ajax()) {
            //清除缓存
            delcache('cacheSystemConfig');
        }
    }

}
