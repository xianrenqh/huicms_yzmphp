<?php

/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-02-02
 * Time: 16:04:26
 * Info:
 */
defined('IN_YZMPHP') or exit('Access Denied');
yzm_base::load_controller('common', 'admin', 0);

class uplibrary extends common
{

    /**
     * 文件库列表
     */
    public function public_file_list()
    {
        if (is_ajax()) {
            // 文件列表
            $group_id = (int)input('post.group_id', -1);
            $page     = (int)input('post.page', 1);
            $limit    = (int)input('post.pageSize', 18);
            $first    = intval(($page - 1) * $limit);

            $where = "is_delete = 0";
            if (isset($group_id) && $group_id != '-1') {
                $where .= " and group_id=".$group_id;
            }

            $total    = D('upload_file')->where($where)->total();
            $list     = D('upload_file')->where($where)->limit("{$first},{$limit}")->order('id desc')->select();
            $lastPage = intval(ceil($total / $limit));
            $perPage  = intval($page - 1);

            $file_list = [
                'current_page' => $page,
                'last_page'    => $lastPage,
                'per_page'     => $perPage,
                'total'        => $total,
                'file_list'    => $list
            ];
            return_success('ok', $file_list);
        }
    }

    /**
     * 分组列表
     * @return mixed
     */
    public function public_group_list()
    {
        $list = D('upload_group')->select();

        return_success('ok', $list);
    }

    /**
     * 新增分组
     */
    public function public_add_group()
    {
        if (is_post()) {
            $group_name = input('post.group_name');
            $group_type = input('post.group_type', 'image');
            $model      = D('upload_group')->where(['group_name' => $group_name])->find();
            if ( ! empty($model)) {
                return_error('分组名称已存在');
            }
            $data     = [
                'group_type'  => $group_type,
                'group_name'  => $group_name,
                'create_time' => time(),
                'update_time' => time(),
            ];
            $rowId    = D('upload_group')->insert($data);
            $group_id = $rowId;
            if ($rowId) {
                return_success("添加成功", compact('group_id', 'group_name'));
            } else {
                return_error('添加失败');
            }
        }
    }

    /**
     * 编辑分组
     */
    public function public_edit_group()
    {
        $group_id   = input('post.group_id');
        $group_name = input('post.group_name');

        $getRow = D('upload_group')->find($group_id);
        if (empty($getRow)) {
            return_error('获取数据失败');
        }
        $row = D('upload_group')->where(['group_id' => $group_id])->update(['group_name' => $group_name]);
        if ($row) {
            return_success('更新成功');
        } else {
            return_error('更新失败');
        }
    }

    /**
     * 删除分组
     */
    public function public_delete_group()
    {
        $group_id = input('post.group_id');
        if (empty($group_id)) {
            error_json('分组id不能为空');
        }
        $del = D('upload_group')->delete(['group_id' => $group_id]);
        if ($del) {
            return_success('删除成功');
        } else {
            return_error('删除失败');
        }
    }

    /**
     * 删除文件
     */
    public function public_delete_files()
    {
        $fieldIds = input('post.fileIds');
        if (empty($fieldIds)) {
            error_json('您没有选择任何文件~');
        }
        $fieldIds = implode(",", $fieldIds);
        D('upload_file')->where("id in ($fieldIds)")->update(['is_delete' => 1]);

        return_success('软删除成功~');
    }

    /**
     * 批量移动文件分组
     */
    public function public_move_files()
    {
        $groupId  = input('post.group_id', 0);
        $fieldIds = input('post.fileIds');
        if (empty($fieldIds)) {
            error_json('您没有选择任何文件~');
        }
        $fieldIds = implode(",", $fieldIds);
        D('upload_file')->where("id in ($fieldIds)")->update(['group_id' => $groupId]);

        return_success('移动分组成功~');
    }

}
