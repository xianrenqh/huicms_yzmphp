<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-01-29
 * Time: 11:27:52
 * Info: 权限菜单控制器
 */
defined('IN_YZMPHP') or exit('Access Denied');
yzm_base::load_controller('common', 'admin', 0);
yzm_base::load_model("menu_model", "admin");

class menu extends common
{

    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = D('auth_rule');
    }

    public function init()
    {
        if (is_ajax()) {
            $list  = $this->model->where(['delete_time' => 0])->order("sort asc,id asc")->select();
            $count = count($list);
            return_json(['code' => 0, 'msg' => 'ok', 'data' => $list, 'count' => $count]);
        }

        include $this->admin_tpl('menu/index');
    }

    /**
     * 添加菜单
     * @return void
     */
    public function add()
    {
        if (is_post()) {
            $param = input('post.');
            if (empty($param['pid'])) {
                $param['pid'] = 0;
            }
            if (empty($param['title'])) {
                return_error("权限名称不能为空");
            }
            if (empty($param['node'])) {
                return_error("权限地址不能为空");
            }
            $param['pid']         = (int)remove_xss($param['pid']);
            $param['type']        = (int)remove_xss($param['type']);
            $param['is_menu']     = (int)remove_xss($param['is_menu']);
            $param['title']       = remove_xss($param['title']);
            $param['node']        = strtolower(remove_xss($param['node']));
            $param['icon']        = ! empty($param['icon']) ? remove_xss($param['icon']) : 'layui-icon-circle-dot';
            $param['sort']        = (int)remove_xss($param['sort']);
            $param['create_time'] = SYS_TIME;
            $param['update_time'] = SYS_TIME;

            if (strpos($param['node'], "/") !== 0) {
                $param['node'] = "/".$param['node'];
            }
            //查询是否已存在
            $findData = $this->model->where(['node' => $param['node']])->find();
            if ( ! empty($findData)) {
                return_error("该权限地址已存在");
            }

            $this->model->insert($param);
            $adminId = cmf_get_admin_id();
            delcache('cacheAdminMenuId'.$adminId);
            return_success('保存成功');
        }
        $id      = input('get.id');
        $pidList = menu_model::getPidMenuList();
        include $this->admin_tpl('menu/add');
    }

    public function edit()
    {
        if (is_post()) {
            $param = input('post.');
            if (empty($param['pid'])) {
                $param['pid'] = 0;
            }
            if (empty($param['title'])) {
                return_error("权限名称不能为空");
            }
            if (empty($param['node'])) {
                return_error("权限地址不能为空");
            }
            $param['id']          = (int)remove_xss($param['id']);
            $param['pid']         = (int)remove_xss($param['pid']);
            $param['type']        = (int)remove_xss($param['type']);
            $param['is_menu']     = (int)remove_xss($param['is_menu']);
            $param['title']       = remove_xss($param['title']);
            $param['node']        = remove_xss($param['node']);
            $param['icon']        = ! empty($param['icon']) ? remove_xss($param['icon']) : 'layui-icon-circle-dot';
            $param['sort']        = (int)remove_xss($param['sort']);
            $param['update_time'] = SYS_TIME;

            if (strpos($param['node'], "/") !== 0) {
                $param['node'] = "/".$param['node'];
            }
            //查询是否已存在
            $findData = $this->model->where(['node' => $param['node'], 'id<>' => $param['id']])->find();
            if ( ! empty($findData)) {
                return_error("该权限地址已存在");
            }

            $this->model->update($param, ['id' => $param['id']]);
            $adminId = cmf_get_admin_id();
            delcache('cacheAdminMenuId'.$adminId);
            return_success('保存成功');
        }
        $id   = input('get.id');
        $data = $this->model->where(['id' => $id])->find();
        if (empty($data)) {
            showmsg('获取数据失败','stop');
        }
        $pidList = menu_model::getPidMenuList();
        include $this->admin_tpl('menu/edit');
    }

    public function delete()
    {
        $id = (int)input('get.id');
        if (empty($id)) {
            return_error("参数错误");
        }
        //查询数据
        $findData = $this->model->where(['id' => $id])->find();
        if (empty($findData)) {
            return_error("获取数据失败，该数据不存在！");
        }
        //查询子级是否存在
        $findPidData = $this->model->where(['pid' => $id])->total();
        if ($findPidData > 0) {
            return_error("请先删除子级后再操作！");
        }
        $this->model->delete(['id' => $id]);
        $adminId = cmf_get_admin_id();
        delcache('cacheAdminMenuId'.$adminId);
        return_success("删除成功");

    }

    public function modify()
    {
        $id    = (int)input('post.id');
        $val   = (int)input('post.val');
        $field = input('post.field');
        if (empty($id)) {
            return_error('参数错误');
        }
        $this->model->update([$field => $val, 'update_time' => time()], ['id' => $id]);
        $adminId = cmf_get_admin_id();
        delcache('cacheAdminMenuId'.$adminId);
        return_success('操作成功');
    }

}
