<?php

/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-04-02
 * Time: 18:31:40
 * Info:
 */
defined('IN_YZMPHP') or exit('Access Denied');
yzm_base::load_controller('common', 'admin', 0);
yzm_base::load_model("auth_group_model", "admin");

class site_model extends common
{

    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = D('site_model');
    }

    /**
     * 模型列表
     */
    public function init()
    {
        $list = $this->model->where(['siteid' => self::$siteid, 'type' => 0])->order('modelid ASC')->select();
        include $this->admin_tpl('site_model/index');
    }

}