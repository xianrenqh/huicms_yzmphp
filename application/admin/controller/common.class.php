<?php
session_start();
yzm_base::load_sys_class('auth', '', 0);

class common
{

    public static $siteid;

    public function __construct()
    {
        self::$siteid = get_siteid();
        $errorNumTips = config('auth.error_num_tips');
        $res          = self::check_admin();
        if (isset($res) && ! empty($errorNumTips[$res])) {
            if ($res != 1 || ! $res == 200) {
                if ($res == 403) {
                    if (is_ajax()) {
                        return_error($errorNumTips[$res]);
                    } else {
                        die($errorNumTips[$res]);
                    }
                } else {
                    showmsg($errorNumTips[$res], U("admin/index/login"), 0);
                }
            }
            //更新session
            set_session("admin_id", cmf_get_admin_id());
        } else {
            showmsg($errorNumTips[$res], U("admin/index/login"), 0);
        }
        //写入操作日志
        $this->adminManageLog();
    }

    /**
     * 判断用户是否已经登陆
     */
    public function check_admin()
    {
        $adminId     = cmf_get_admin_id();
        $currentNode = "/".ROUTE_M."/".ROUTE_C."/".ROUTE_A;
        //验证登录
        if ( ! in_array($currentNode, config('auth.no_auth_node')) && ! in_array(ROUTE_C,
                config('auth.no_login_controller'))) {
            if (empty(get_session('admin_id'))) {
                return 401;
            }
            if ($adminId != get_session('admin_id')) {
                return 402;
            }
        }

        //验证权限
        if ( ! in_array($currentNode, config('auth.no_auth_node')) && ! in_array(ROUTE_C,
                config('auth.no_auth_controller'))) {
            $checkNode = auth::instance()->check($currentNode, $adminId);
            if ( ! $checkNode) {
                return 403;
            }
        }

        return self::check_referer();
    }

    /**
     * 加载后台模板
     *
     * @param string $file 文件名
     * @param string $m    模型名
     */
    public static function admin_tpl($file, $m = '')
    {
        $m = empty($m) ? ROUTE_M : $m;
        if (empty($m)) {
            return false;
        }

        return APP_PATH.$m.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR.$file.'.html';
    }

    /**
     * 检查REFERER
     */
    private static function check_referer()
    {
        if (strpos(ROUTE_A, 'public_') === 0) {
            return 1;
        }
        if (HTTP_REFERER) {
            if (strpos(HTTP_REFERER, SERVER_PORT.HTTP_HOST) === false) {
                return 501;
            }
        }

        return 1;
    }

    /**
     * 写入操作日志
     * @return void
     */
    private function adminManageLog()
    {
        if ( ! get_config('admin_log')) {
            return false;
        }
        if (ROUTE_A == '' || ROUTE_A == 'init' || ROUTE_A == 'index' || strpos(ROUTE_A, '_list') || in_array(ROUTE_A,
                array('login', 'public_welcome')) || is_ajax() === false) {
            return false;
        }
        $data = [
            'module'      => ROUTE_M,
            'controller'  => ROUTE_C,
            'admin_name'  => $_SESSION['admin_name']['data'],
            'admin_id'    => $_SESSION['admin_id']['data'],
            'querystring' => http_build_query($_GET),
            'log_time'    => SYS_TIME,
            'ip'          => getip(),
            'create_time' => SYS_TIME
        ];
        D('admin_log')->insert($data);
    }

    /**
     * 封装成功返回
     *
     * @param $msg
     * @param $data
     * @param $count
     *
     * @return void
     */
    public function success($msg = '', $data = '', $count = 0)
    {
        $result = [
            'code'     => 200,
            'msg'      => $msg,
            'data'     => $data,
            'count'    => $count,
            'lay_code' => 0     //layui数据表格返回数据code
        ];
        return_json($result);
    }

    /**
     * 封装失败返回
     *
     * @param $msg
     * @param $data
     *
     * @return void
     */
    public function error($msg = '', $data = '')
    {
        $result = [
            'code' => 0,
            'msg'  => $msg,
            'data' => $data
        ];
        return_json($result);
    }

}
