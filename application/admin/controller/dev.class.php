<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-02-02
 * Time: 14:08:14
 * Info: 开发辅助
 */
defined('IN_YZMPHP') or exit('Access Denied');
yzm_base::load_controller('common', 'admin', 0);
yzm_base::load_sys_class('form','',0);

class dev extends common
{

    public function upload_test()
    {
        include $this->admin_tpl('dev/upload_test');
    }

}
