<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-02-14
 * Time: 15:59:54
 * Info:
 */
defined('IN_YZMPHP') or exit('Access Denied');
yzm_base::load_controller('common', 'admin', 0);
yzm_base::load_model("category_model", "admin");

class category extends common
{

    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = D('category');
    }

    public function init()
    {
        if (is_ajax()) {
            $list  = $this->model->where(['delete_time' => 0])->order("sort asc,id asc")->select();
            $count = count($list);
            return_json(['code' => 0, 'msg' => 'ok', 'data' => $list, 'count' => $count]);
        }
        include $this->admin_tpl('category/index');
    }

    public function add()
    {
        if (is_ajax()) {
            $param = input('post.');
            if (empty($param['name'])) {
                return_error("栏目名称不能为空哦");
            }
            $param['name']        = remove_xss($param['name']);
            $param['name_en']     = remove_xss($param['name_en']);
            $param['parent_id']   = (int)($param['parent_id']);
            $param['sort']        = (int)($param['sort']);
            $param['is_menu']     = (int)($param['is_menu']);
            $param['status']      = (int)($param['status']);
            $param['create_time'] = time();
            $param['update_time'] = time();
            $this->model->insert($param);
            return_success('保存成功');
        }
        $id      = (int)input('get.id');
        $pidList = category_model::getPidMenuList();
        include $this->admin_tpl('category/add');
    }

    public function edit()
    {
        if (is_ajax()) {
            $param = input('post.');
            if (empty($param['name'])) {
                return_error("栏目名称不能为空哦");
            }
            $param['name']        = remove_xss($param['name']);
            $param['name_en']     = remove_xss($param['name_en']);
            $param['parent_id']   = (int)($param['parent_id']);
            $param['sort']        = (int)($param['sort']);
            $param['is_menu']     = (int)($param['is_menu']);
            $param['status']      = (int)($param['status']);
            $param['update_time'] = time();
            $this->model->update($param, ['id' => $param['id']]);
            return_success('保存成功');
        }
        $id      = (int)input('get.id');
        $data    = $this->model->where(['id' => $id])->find();
        $pidList = category_model::getPidMenuList();
        include $this->admin_tpl('category/edit');
    }

    public function delete()
    {
        $id = (int)input('get.id');
        if (empty($id)) {
            return_error("参数错误");
        }
        //查询数据
        $findData = $this->model->where(['id' => $id])->find();
        if (empty($findData)) {
            return_error("获取数据失败，该数据不存在！");
        }
        //查询子级是否存在
        $findPidData = $this->model->where(['parent_id' => $id])->total();
        if ($findPidData > 0) {
            return_error("请先删除子级后再操作！");
        }
        $this->model->delete(['id' => $id]);
        return_success("删除成功");
    }

    public function modify()
    {
        if (is_ajax()) {
            $id    = (int)input('id');
            $val   = (int)input('val');
            $field = input('field');
            if (empty($id)) {
                return_error('参数错误');
            }
            $this->model->update([$field => $val, 'update_time' => time()], ['id' => $id]);
            return_success('操作成功');
        }
    }

}
