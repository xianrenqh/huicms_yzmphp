<?php
/**
 * form.class.php  form类
 *
 * @author           袁志蒙
 * @license          http://www.yzmcms.com
 * @lastmodify       2016-12-10
 */

class form
{

    /**
     * 默认表单-备份代码
     * 未启用
     *
     * @param $name
     * @param $value
     * @param $required
     * @param $attribute
     *
     * @return string
     */
    public static function text_bak($name = '', $value = '', $required = false, $attribute = '')
    {
        $isRequired = $required ? "lay-verify='required'" : '';
        $string     = '<div class="layui-input-block">';
        $string     .= '';
        $string     .= '</div>';

        return $string;
    }

    /**
     * 编辑器
     *
     * @param $name   name
     * @param $val    默认值
     * @param $style  样式
     * @param $isload 是否加载js,当该页面加载过编辑器js后，无需重复加载
     */
    public static function editor($name = 'content', $val = '', $style = '', $isload = false)
    {
        $val        = htmlspecialchars_decode($val);
        $editorType = get_config('site_editor');
        $res        = '';
        switch ($editorType) {
            case 'uEditorPlus';
                $res = self::editor_uEditorPlus($name, $val, $style);
                break;
            case 'sdEditor';
                $res = self::editor_sdEditor($name, $val, $style);
                break;
        }

        return $res;
    }

    private static function editor_uEditorPlus($name, $val, $style = '')
    {
        $toolbars1 = [];
        $toolbars  = get_config('ueditor_icon');
        $toolbars  = array_filter(explode(',', $toolbars));
        foreach ($toolbars as $key => $item) {
            if (is_int($key / 5) && $key > 0) {
                array_push($toolbars1, "|");
                array_push($toolbars1, $toolbars[$key]);
            } else {
                array_push($toolbars1, $toolbars[$key]);
            }
        }

        if ( ! empty($toolbars1)) {
            $toolbars2 = 'toolbars:['.json_encode($toolbars1).']';
        } else {
            $toolbars2 = "";
        }

        $configJs = STATIC_URL.'lib'.DIRECTORY_SEPARATOR.'ueditor-plus-2.8'.DIRECTORY_SEPARATOR.'ueditor.config.js';
        $Js2      = STATIC_URL.'lib'.DIRECTORY_SEPARATOR.'ueditor-plus-2.8'.DIRECTORY_SEPARATOR.'ueditor.all.js';
        $lang     = STATIC_URL.'lib'.DIRECTORY_SEPARATOR.'ueditor-plus-2.8'.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.'zh-cn'.DIRECTORY_SEPARATOR.'zh-cn.js';
        $string   = '';
        $string   .= '<script id="container" name="'.$name.'" type="text/plain" style="'.$style.'" >'.$val.'</script>';
        $string   .= '<script type="text/javascript" src="'.$configJs.'"></script>';
        $string   .= '<script type="text/javascript" src="'.$Js2.'"></script>';
        $string   .= '<script type="text/javascript" src="'.$lang.'"></script>';
        $string   .= '<script type="text/javascript">';
        $string   .= 'var ue = UE.getEditor("container",{';
        $string   .= $toolbars2;
        $string   .= '});';
        $string   .= '</script>';

        return $string;

    }

    private static function editor_sdEditor($name, $val, $style = '')
    {
        $Jquery = STATIC_URL.'lib'.DIRECTORY_SEPARATOR.'sdEditor'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR."jquery.js";
        $Js2    = STATIC_URL.'lib'.DIRECTORY_SEPARATOR.'sdEditor'.DIRECTORY_SEPARATOR.'editor'.DIRECTORY_SEPARATOR.'editor.js';
        $string = '';
        $string .= '<script type="text/javascript" charset="utf-8" src="'.$Jquery.'"></script>';
        $string .= '<script type="text/javascript" charset="utf-8" src="'.$Js2.'"></script>';
        //$string .= '<div id="'.$name.'" style="'.$style.'"></div>';
        $string .= '<textarea id="'.$name.'" name="'.$name.'">'.$val.'</textarea>';
        $string .= "<script>
                $(function()
                    {
                        $('#{$name}').editor({
                        toolbar:'full',
                        /*value: '{$val}',*/
                        upload:'/common/uploads/upload?editor_type=sdEditor',
                        });
                    });
                </script>
                ";

        return $string;
    }

    /**
     * 编辑器-Mini版
     *
     * @param $name   name
     * @param $val    默认值
     * @param $style  样式
     * @param $isload 是否加载js,当该页面加载过编辑器js后，无需重复加载
     */
    public static function editor_mini($name = 'content', $val = '', $style = '', $isload = false)
    {

    }

}
