<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-01-17
 * Time: 14:12:42
 * Info: auth权限验证配置
 */
return [
    //不需要验证节点路由的，一定要在最前面加上斜杠
    'no_auth_node'        => [
        '/admin/index/login',
        '/admin/index/login_out',
        '/admin/index/init',
        '/admin/index/menu',
        '/admin/index/file_list',
        '/admin/index/cacheClear',
    ],
    //不需要验证登录控制器的
    'no_login_controller' => [
        'login'
    ],
    //不需要验证权限控制器的
    'no_auth_controller'  => [
        'login',
        'upload'
    ],
    //错误码提示
    'error_num_tips'      => [
        '1'   => '成功',
        '200' => '成功',
        '401' => '请先登录',
        '402' => '登录已过期，请重新登录',
        '403' => '无访问权限',
        '501' => 'REFERER错误'
    ]
];
