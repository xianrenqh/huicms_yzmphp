<?php
/**
 * Created by PhpStorm.
 * User: 小灰灰
 * Date: 2023-04-02
 * Time: 18:35:11
 * Info:
 */
/**
 * 获取当前的站点ID
 * 兼容站群模块
 */
function get_siteid()
{

    if ( ! is_file(APP_PATH.'site/common/function/function.php')) {
        return 0;
    }

    include_once APP_PATH.'site/common/function/function.php';

    return public_get_siteid();
}