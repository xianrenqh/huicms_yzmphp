HuiCMF-yzm后台管理系统
>> 基于YzmPHP框架开发的huicmf_yzm后台管理系统

## 后台常用缓存

1. 后台配置缓存：
   cacheSystemConfig
2. 后台菜单缓存：
   cacheAdminMenuId+(用户id)
3. 后台权限缓存：
   cache_auth_rules

# YZMPHP

YZMPHP V2.8 - 轻量级开源PHP框架

支持PHP5.4+~PHP8.1，推荐PHP7、PHP8。

YZMPHP框架是由袁志蒙自主研发的一款轻量级开源PHP框架,该框架主要特点：简洁轻巧、MVC设计模式、模块式开发、容易上手，是一款值得学习使用的框架。

>YZMPHP 免费·开源·轻量级

SITE: www.yzmphp.com

