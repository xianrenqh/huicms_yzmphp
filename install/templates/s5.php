<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $Title; ?> - <?php echo $Powered; ?></title>
        <link rel="stylesheet" href="./css/install.css?v=huicmf" />
    </head>
    <body>
        <div class="wrap">
            <?php require './templates/header.php'; ?>
            <section class="section">
                <div class="success_tip cc">
                    <a href="<?php echo $domain ?>admin/index/login" class="hui_doadmin">安装完成，进入后台管理</a>
                </div>
            </section>
        </div>
        <?php require './templates/footer.php'; ?>
    </body>
</html>
