/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : 127.0.0.1:3306
 Source Schema         : huicmf_yzmphp

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 31/03/2023 16:30:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hui_admin
-- ----------------------------
DROP TABLE IF EXISTS `hui_admin`;
CREATE TABLE `hui_admin`  (
  `id` mediumint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `role_id` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `real_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `login_time` int(10) UNSIGNED NULL DEFAULT 0,
  `login_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `status` int(1) NOT NULL COMMENT '状态',
  `err_num` int(1) NOT NULL DEFAULT 0,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `update_time` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hui_admin
-- ----------------------------
INSERT INTO `hui_admin` VALUES (1, 'admin', '844f2bd9526f5abddc94566a92f40871', 'BEPoaA', 1, '超级管理员', '超管1', '/uploads/202302/17/230217035650743.jpg', 'test@gmail.com', 1680247929, '127.0.0.1', 1, 0, 1676609802, 1680247929);
INSERT INTO `hui_admin` VALUES (2, 'test', '213d59569df83c52e042d0e14bb45fb0', 'DPJuwe', 2, '', 'test', '', 'test@gmail.com', 1680156809, '127.0.0.1', 1, 0, 1680148812, 1680156809);

-- ----------------------------
-- Table structure for hui_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `hui_admin_log`;
CREATE TABLE `hui_admin_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `controller` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `querystring` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `admin_id` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `admin_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `log_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `create_time` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `logtime`(`log_time`) USING BTREE,
  INDEX `adminid`(`admin_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hui_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for hui_admin_login_log
-- ----------------------------
DROP TABLE IF EXISTS `hui_admin_login_log`;
CREATE TABLE `hui_admin_login_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `login_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `login_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `login_result` tinyint(1) NOT NULL DEFAULT 0 COMMENT '登录结果1为登录成功0为登录失败',
  `cause` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `create_time` int(10) NOT NULL DEFAULT 0,
  `update_time` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hui_admin_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for hui_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `hui_auth_group`;
CREATE TABLE `hui_auth_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父组别',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组名',
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '规则ID',
  `createtime` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `updatetime` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分组表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of hui_auth_group
-- ----------------------------
INSERT INTO `hui_auth_group` VALUES (1, 0, '超级管理员', '*', 1623383000, 1623383000, 'normal');
INSERT INTO `hui_auth_group` VALUES (2, 0, '管理员', '1,7,11,12,17,30,31,32', 1623383000, 1623383000, 'normal');

-- ----------------------------
-- Table structure for hui_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `hui_auth_group_access`;
CREATE TABLE `hui_auth_group_access`  (
  `uid` int(10) UNSIGNED NOT NULL COMMENT '会员ID',
  `group_id` int(10) UNSIGNED NOT NULL COMMENT '级别ID',
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限分组表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of hui_auth_group_access
-- ----------------------------
INSERT INTO `hui_auth_group_access` VALUES (1, 1);
INSERT INTO `hui_auth_group_access` VALUES (2, 2);

-- ----------------------------
-- Table structure for hui_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `hui_auth_rule`;
CREATE TABLE `hui_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(10) NOT NULL DEFAULT 0 COMMENT '节点类型（1：控制器，2：节点）',
  `node` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `is_auth` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否启动RBAC权限控制',
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '条件',
  `is_menu` int(1) NOT NULL DEFAULT 0 COMMENT '是否菜单',
  `pid` int(10) NOT NULL DEFAULT 0,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `sort` int(10) NOT NULL DEFAULT 100,
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`node`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '节点表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of hui_auth_rule
-- ----------------------------
INSERT INTO `hui_auth_rule` VALUES (1, 0, '/system', '系统管理', 1, '', 1, 0, 'layui-icon-set', 100, 1622011952, 1674988339, 0);
INSERT INTO `hui_auth_rule` VALUES (2, 1, '/admin/menu/init', '权限管理', 1, '', 1, 1, 'layui-icon-auz', 100, 1622011952, 1676427147, 0);
INSERT INTO `hui_auth_rule` VALUES (3, 2, '/admin/menu/add', '添加权限', 1, '', 0, 2, 'layui-icon-rate', 100, 1622011952, 1674982450, 0);
INSERT INTO `hui_auth_rule` VALUES (4, 2, '/admin/menu/edit', '编辑权限', 1, '', 0, 2, 'layui-icon-circle-dot', 100, 1674984430, 1675065946, 0);
INSERT INTO `hui_auth_rule` VALUES (5, 2, '/admin/menu/delete', '删除权限', 1, '', 0, 2, 'layui-icon-circle-dot', 100, 1674984444, 1674984444, 0);
INSERT INTO `hui_auth_rule` VALUES (6, 1, '/admin/authgroup/init', '角色组管理', 1, '', 1, 1, 'layui-icon-user', 100, 1674988218, 1674988218, 0);
INSERT INTO `hui_auth_rule` VALUES (7, 1, '/admin/admin/init', '管理员管理', 1, '', 1, 1, 'layui-icon-username', 100, 1675066218, 1675066227, 0);
INSERT INTO `hui_auth_rule` VALUES (8, 1, '/admin/authgroup/add', '添加角色组', 1, '', 0, 6, 'layui-icon-circle-dot', 100, 1675157318, 1675157318, 0);
INSERT INTO `hui_auth_rule` VALUES (9, 1, '/admin/authgroup/edit', '编辑角色组', 1, '', 0, 6, 'layui-icon-circle-dot', 100, 1675157333, 1675157333, 0);
INSERT INTO `hui_auth_rule` VALUES (10, 1, '/admin/authgroup/delete', '删除角色组', 1, '', 0, 6, 'layui-icon-circle-dot', 100, 1675157344, 1675157344, 0);
INSERT INTO `hui_auth_rule` VALUES (11, 1, '/admin/admin/add', '添加管理员', 1, '', 0, 7, 'layui-icon-circle-dot', 100, 1675157385, 1675157385, 0);
INSERT INTO `hui_auth_rule` VALUES (12, 1, '/admin/admin/edit', '编辑管理员', 1, '', 0, 7, 'layui-icon-circle-dot', 100, 1675157398, 1675157398, 0);
INSERT INTO `hui_auth_rule` VALUES (13, 0, '/admin/admin/delete', '删除管理员', 1, '', 0, 7, 'layui-icon-circle-dot', 100, 1675157406, 1675157406, 0);
INSERT INTO `hui_auth_rule` VALUES (14, 1, '/admin/admin/status', '更改状态', 1, '', 0, 7, 'layui-icon-circle-dot', 100, 1675159173, 1675159173, 0);
INSERT INTO `hui_auth_rule` VALUES (15, 1, '/admin/config/init', '系统设置', 1, '', 1, 1, 'layui-icon-set-fill', 100, 1675308836, 1675308836, 0);
INSERT INTO `hui_auth_rule` VALUES (16, 1, '/admin/config/save', '保存设置', 1, '', 0, 15, 'layui-icon-circle-dot', 100, 1675310396, 1675310396, 0);
INSERT INTO `hui_auth_rule` VALUES (17, 1, '/admin/config/customConfig', '自定义设置', 1, '', 1, 1, 'layui-icon-set', 100, 1675821534, 1680158062, 0);
INSERT INTO `hui_auth_rule` VALUES (18, 0, '/content', '内容管理', 1, '', 1, 0, 'layui-icon-read', 10, 1676269530, 1676270059, 0);
INSERT INTO `hui_auth_rule` VALUES (19, 1, '/admin/content/init', '内容管理', 1, '', 1, 18, 'layui-icon-align-center', 100, 1676269583, 1676270139, 0);
INSERT INTO `hui_auth_rule` VALUES (20, 1, '/admin/category/init', '栏目管理', 1, '', 1, 18, 'layui-icon-form', 100, 1676269651, 1676269714, 0);
INSERT INTO `hui_auth_rule` VALUES (21, 1, '/admin/sitemodel/init', '模型管理', 1, '', 0, 18, 'layui-icon-template-1', 100, 1676269685, 1676362570, 0);
INSERT INTO `hui_auth_rule` VALUES (22, 1, '/admin/content/add', '添加内容', 1, '', 0, 19, 'layui-icon-circle-dot', 100, 1676362470, 1676362470, 0);
INSERT INTO `hui_auth_rule` VALUES (23, 1, '/admin/content/edit', '修改内容', 1, '', 0, 19, 'layui-icon-circle-dot', 100, 1676362481, 1676362481, 0);
INSERT INTO `hui_auth_rule` VALUES (24, 1, '/admin/content/delete', '删除内容', 1, '', 0, 19, 'layui-icon-circle-dot', 100, 1676362492, 1676362492, 0);
INSERT INTO `hui_auth_rule` VALUES (25, 1, '/admin/category/add', '添加栏目', 1, '', 0, 20, 'layui-icon-circle-dot', 100, 1676362511, 1676362511, 0);
INSERT INTO `hui_auth_rule` VALUES (26, 1, '/admin/category/edit', '修改栏目', 1, '', 0, 20, 'layui-icon-circle-dot', 100, 1676362522, 1676362522, 0);
INSERT INTO `hui_auth_rule` VALUES (27, 0, '/admin/category/delete', '删除栏目', 1, '', 0, 20, 'layui-icon-circle-dot', 100, 1676362534, 1676362534, 0);
INSERT INTO `hui_auth_rule` VALUES (28, 1, '/admin/category/modify', '列表操作', 1, '', 0, 20, 'layui-icon-circle-dot', 100, 1676427956, 1676427956, 0);
INSERT INTO `hui_auth_rule` VALUES (29, 1, '/admin/menu/modify', '列表操作', 1, '', 0, 2, 'layui-icon-circle-dot', 100, 1676427990, 1676427990, 0);
INSERT INTO `hui_auth_rule` VALUES (30, 1, '/admin/config/custom_config_add', '添加自定义设置', 1, '', 0, 17, 'layui-icon-circle-dot', 100, 1680148706, 1680148706, 0);
INSERT INTO `hui_auth_rule` VALUES (31, 1, '/admin/config/custom_config_edit', '修改自定义设置', 1, '', 0, 17, 'layui-icon-circle-dot', 100, 1680148720, 1680148774, 0);
INSERT INTO `hui_auth_rule` VALUES (32, 1, '/admin/config/custom_config_delete', '删除自定义设置', 1, '', 0, 17, 'layui-icon-circle-dot', 100, 1680148745, 1680148784, 0);
INSERT INTO `hui_auth_rule` VALUES (33, 1, '/admin/log/loginloglist', '后台登录日志', 1, '', 1, 1, 'layui-icon-circle-dot', 200, 1680248004, 1680248022, 0);
INSERT INTO `hui_auth_rule` VALUES (34, 1, '/admin/log/manageloglist', '后台操作日志', 1, '', 1, 1, 'layui-icon-circle-dot', 200, 1680248056, 1680248056, 0);
INSERT INTO `hui_auth_rule` VALUES (35, 1, '/admin/log/loginlogdel', '删除登录日志', 1, '', 0, 33, 'layui-icon-circle-dot', 100, 1680250976, 1680250976, 0);
INSERT INTO `hui_auth_rule` VALUES (36, 1, '/admin/log/managelogdel', '删除操作日志', 1, '', 0, 34, 'layui-icon-circle-dot', 100, 1680251011, 1680251011, 0);

-- ----------------------------
-- Table structure for hui_category
-- ----------------------------
DROP TABLE IF EXISTS `hui_category`;
CREATE TABLE `hui_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `catdir` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '英文目录',
  `parent_id` int(10) NOT NULL DEFAULT 0 COMMENT '父级id',
  `sort` int(10) NOT NULL DEFAULT 100 COMMENT '排序，越小越靠前',
  `is_menu` int(1) NOT NULL DEFAULT 1 COMMENT '是否导航显示',
  `image` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '栏目图片',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(10) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hui_category
-- ----------------------------
INSERT INTO `hui_category` VALUES (1, '测试', 'test', 0, 100, 1, '', 1, 1676365425, 1676365425, 0);

-- ----------------------------
-- Table structure for hui_config
-- ----------------------------
DROP TABLE IF EXISTS `hui_config`;
CREATE TABLE `hui_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置类型',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置说明',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置值',
  `fieldtype` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '字段类型',
  `setting` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '字段设置',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  `tips` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hui_config
-- ----------------------------
INSERT INTO `hui_config` VALUES (1, 'site_name', 1, '站点名称', 'HuiCMF后台管理系统', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (2, 'site_url', 1, '站点跟网址', 'http://huicmf_yzm.cc/', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (3, 'admin_log', 3, '启用后台管理操作日志', '0', 'radio', '', 1, '');
INSERT INTO `hui_config` VALUES (4, 'site_keyword', 1, '站点关键字', 'huicmf', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (5, 'site_copyright', 1, '网站版权信息', 'Powered By yzmphp后台系统 © 2019-2023 小灰灰工作室', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (6, 'site_beian', 1, '站点备案号', '豫ICP备666666号', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (7, 'site_description', 1, '站点描述', '我是描述', 'text', '', 1, '');
INSERT INTO `hui_config` VALUES (8, 'site_code', 1, '统计代码', '', 'text', '', 1, '');
INSERT INTO `hui_config` VALUES (9, 'admin_prohibit_ip', 3, '禁止访问网站的IP', 'HuiCMF后台管理系统', 'text', '', 1, '');
INSERT INTO `hui_config` VALUES (10, 'mail_server', 4, 'SMTP服务器', 'smtp.exmail.qq.com', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (11, 'mail_port', 4, 'SMTP服务器端口', '465', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (12, 'mail_user', 4, 'SMTP服务器的用户帐号', '', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (13, 'mail_pass', 4, 'SMTP服务器的用户密码', '', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (14, 'mail_inbox', 4, '收件邮箱地址', '', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (15, 'login_captcha', 2, '后台登录验证码', '1', 'string', NULL, 1, '');
INSERT INTO `hui_config` VALUES (16, 'watermark_enable', 2, '是否开启图片水印', '0', 'radio', '', 1, '');
INSERT INTO `hui_config` VALUES (17, 'watermark_name', 2, '水印图片名称', 'mark.png', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (18, 'watermark_position', 2, '水印的位置', '2', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (19, 'watermark_touming', 2, '水印透明度', '66', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (20, 'upload_types_image', 2, '允许上传图片类型', 'jpg,jpeg,png,gif,bmp', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (21, 'upload_mode', 2, '图片上传方式', '1', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (22, 'site_editor', 1, '文本编辑器', 'sdEditor', 'string', ' ', 1, '');
INSERT INTO `hui_config` VALUES (23, 'upload_types_file', 2, '允许上传附件类型', 'zip,pdf,doc,txt,json', 'string', ' ', 1, '');
INSERT INTO `hui_config` VALUES (24, 'admin_url_password', 3, '后台加密码', 'admin', 'string', ' ', 1, ' ');
INSERT INTO `hui_config` VALUES (25, 'dingxiang_appid', 2, '顶象验证appid', '', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (26, 'dingxiang_appSecret', 2, '顶象验证appSecret', '', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (27, 'dingxiang_apiurl', 2, '顶象验证-api地址', 'https://api-pro.dingxiang-inc.com', 'string', '', 1, '');
INSERT INTO `hui_config` VALUES (28, 'ueditor_icon', 5, 'ueditor图标显示', 'fullscreen,source,undo,redo,bold,italic,underline,fontborder,strikethrough,superscript,subscript,removeformat,formatmatch,autotypeset,blockquote,pasteplain,forecolor,backcolor,insertorderedlist,insertunorderedlist,selectall,cleardoc,rowspacingtop,rowspacingbottom,lineheight,customstyle,paragraph,fontfamily,fontsize,directionalityltr,directionalityrtl,indent,justifyleft,justifycenter,justifyright,justifyjustify,touppercase,tolowercase,link,unlink,anchor,imagenone,imageleft,imageright,imagecenter,simpleupload,insertimage,emotion,scrawl,insertvideo,attachment,insertframe,insertcode,pagebreak,template,background,horizontal,date,time,spechars,wordimage,inserttable,deletetable,insertparagraphbeforetable,insertrow,deleterow,insertcol,deletecol,mergecells,mergeright,mergedown,splittocells,splittorows,splittocols,preview,searchreplace,help', 'text', NULL, 1, '');
INSERT INTO `hui_config` VALUES (29, 'pic_more_nums', 2, '多图上传图片数量限制', '0', 'string', NULL, 1, '');
INSERT INTO `hui_config` VALUES (30, 'upload_maxsize', 2, '最大上传大小', '2048', 'string', NULL, 1, '');
INSERT INTO `hui_config` VALUES (31, 'test', 99, '', 'test', 'textarea', '', 1, '');
INSERT INTO `hui_config` VALUES (32, 'test_image', 99, '图片12', '/uploads/202302/10/230210060646116.jpg', 'image', '', 1, '');
INSERT INTO `hui_config` VALUES (33, 'test_radio', 99, '性别测试', '3', 'radio', '[\"\\u7537:1\", \"\\u5973:2\", \"\\u4EBA\\u5996:3\"]', 1, '');
INSERT INTO `hui_config` VALUES (34, 'test_select', 99, '性别', '2', 'select', '[\"\\u7537:1\",\"\\u5973:2\",\"\\u4eba\\u5996:3\"]', 1, '');
INSERT INTO `hui_config` VALUES (35, 'test1', 99, 'test1', '2023-02-27 15:11:08', 'datetime', '', 1, '');
INSERT INTO `hui_config` VALUES (36, 'test3', 99, '', 'test3', 'text', '', 1, '');

-- ----------------------------
-- Table structure for hui_content
-- ----------------------------
DROP TABLE IF EXISTS `hui_content`;
CREATE TABLE `hui_content`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL DEFAULT 0 COMMENT '栏目id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文章标题',
  `title_color` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题颜色',
  `subtitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '副标题',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面图地址',
  `flag` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文档属性',
  `jump_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '属性为7的时候的跳转url',
  `weight` int(10) NOT NULL DEFAULT 0 COMMENT '排序，越大越靠前',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '状态：1=发布；0=未发布',
  `seotitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'seo标题',
  `keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'seo关键词',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `source` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '来源',
  `click` int(11) NOT NULL DEFAULT 0 COMMENT '点击数',
  `author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '作者',
  `is_top` int(1) NOT NULL DEFAULT 0 COMMENT '是否置顶：1=是',
  `admin_id` int(10) NOT NULL DEFAULT 0 COMMENT '发布人id',
  `publish_time` int(10) NOT NULL DEFAULT 0 COMMENT '发布时间',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `update_time` int(10) NOT NULL DEFAULT 0 COMMENT '有值为软删除',
  `delete_time` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hui_content
-- ----------------------------
INSERT INTO `hui_content` VALUES (1, 1, '这是标题哦', '', '', '', '1', '', 0, '<a href=\"http://huicmf_yzm.cc/admin/content/add.html\">huicmf后台管理系统 (huicmf_yzm.cc)</a>', 0, '', '这是,标题', 'huicmf后台管理系统 (huicmf_yzm.cc)', '原创', 1, '小编', 1, 1, 1676452034, 1676452034, 1676966096, 0);
INSERT INTO `hui_content` VALUES (2, 1, '今年江苏高考数学难吗？', 'rgba(199, 21, 133, 1)', '这个是副标题', 'https://i03piccdn.sogoucdn.com/9b3b990a53b18c55?.png', '1,2,4,6,7', 'http://huicmf_yzm.cc/admin/content/edit.html?id=2', 0, '<span style=\"color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" -webkit-text-stroke-width:=\"\" 0.05px;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">今年江苏高考数学难吗</span>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">2022江苏高考数学还是比较难的，考的内容非常基础，但是题目创新性非常高，这给很多考生带来了不小的压力。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">高考试卷难度单单从试卷的试题本身来说，这个和每个人的知识点掌握程度和擅长的题目类型有关系，还和个人的临场发挥有关联，高考考生现场状态非常重要。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">江苏高考数学的特点：</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">有网友认为，考题太难把处于中、下水平的大部分考生都筛选落网，混在了一起，不利于体现真实水平。也有网友觉得，本次考题陷阱很多，运算量大，就算能考好，也只能说明是个应试高手，实际应用能力并不见得能真正体现出来，“数学哥在卖弄自己的天才多于筛选人才”。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">不过，网上对于数学哥的评价也并非一边倒，网络上也出现了力撑数学哥的人。“考题难，有区分度更好，不然你好、我好、大家好怎么选拔？高考也就失去意义了。”“大家不用愁，反正是相对分数，录取率还是不会变的，接下来的科目考好了就行。”</p>\r\n<span style=\"color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" -webkit-text-stroke-width:=\"\" 0.05px;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">专业老师在线权威答疑 zy.offercoming.com</span>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"><img src=\"https://i03piccdn.sogoucdn.com/9b3b990a53b18c55?.png\" style=\"box-sizing: border-box; border-style: none; vertical-align: top; height: auto; -webkit-text-stroke: 0.05px !important;\"></p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"></p>\r\n<h3 style=\"box-sizing: border-box; font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" line-height:=\"\" 1.2;=\"\" color:=\"\" rgb(38,=\"\" 38,=\"\" 38);=\"\" margin-top:=\"\" 30px;=\"\" font-size:=\"\" 18px;=\"\" -webkit-text-stroke-width:=\"\" 0.05px;=\"\" padding-left:=\"\" 10px;=\"\" border-left:=\"\" 4px=\"\" solid=\"\" rgb(0,=\"\" 136,=\"\" 204);=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">江苏高考数学难不难,难度系数解读点评解析</h3>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> 2021江苏省高考数学考试已经结束，与往年一样，江苏高考数学又一次登上热搜。那么江苏高考数学试题到底有多难呢，下面我为大家详细介绍一下，供大家参考。</p>\r\n<span style=\"color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" -webkit-text-stroke-width:=\"\" 0.05px;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">江苏高考数学难度</span>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> 江苏省现在的高考模式是自主命题，语数外三门必考，另外，语文和数学各有附加题，文科的考生需要答语文附加题，理科的考生需要答数学附加题。江苏省数学经常有偏题、怪题，150的总分，平均分经常七八十分，甚至还有六十几分的情况。江苏高考再次改革，自2021年起，语数外重回全国卷，江苏的考生彻底欢呼了，他们留下了代表喜悦的眼泪。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> 2021年江苏高考数学卷秉承以往的风格，与课本联系紧密，题目排序由易到难，学生解题时心理状态能够平和，可以发挥出正常水平。试卷考察的知识点全面，注重基础，同时又有区分度，便于高校选拔人才。压轴题延续以往风格，综合性强。整张试卷在强调“通性通法”的前提下，又包含了中学数学知识中所蕴含的基本数学思想方法。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> 给下一届考生的建议：江苏高考的数学题目对基础的考查尤为多，所以在复习时一定要回归教材，夯实基础。同时浙江卷的解析题难度较大，要擅长运用数学思想方法，且要注意解题的规范性。认真去研究往年的试题，就如同与出题者进行对话，可以试着去理解出题者的出题意图。不过之后江苏高考要回归全国卷，希望同学们也要尽早适应全国卷的考查方式。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"><img src=\"https://i02piccdn.sogoucdn.com/328f5574d0a947d8?.png\" style=\"box-sizing: border-box; border-style: none; vertical-align: top; height: auto; -webkit-text-stroke: 0.05px !important;\"></p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"></p>\r\n<h3 style=\"box-sizing: border-box; font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" line-height:=\"\" 1.2;=\"\" color:=\"\" rgb(38,=\"\" 38,=\"\" 38);=\"\" margin-top:=\"\" 30px;=\"\" font-size:=\"\" 18px;=\"\" -webkit-text-stroke-width:=\"\" 0.05px;=\"\" padding-left:=\"\" 10px;=\"\" border-left:=\"\" 4px=\"\" solid=\"\" rgb(0,=\"\" 136,=\"\" 204);=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">2021年江苏高考数学试卷难吗</h3>\r\n<span style=\"color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" -webkit-text-stroke-width:=\"\" 0.05px;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">江苏高考数学难不难,难度系数解读点评解析</span>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"></p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> </p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> 7日下午江苏高考第二科数学考试结束。据考生反馈，今年数学的“压轴题”较难。南京市第三高级中学数学教师范书韵也表示，此次试题有一定区分度，比2013年江苏高考的数学试题要难一些。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> </p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> 范书韵同时指出，今年的数学试题仍然重视基础，考察了8个C级考点，知识点分布与往年一致。解答题前三题，分别考察了三角函数、立体几何、解析几何，相对比较基础、容易上手，从考生反馈的情况看，大部分考生这三题都比较容易上手。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> </p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> 后面的函数导数题、数列题则有一定难度，且每题三个小问之间难度依次增加，想全部答出不容易。此外，往年出现在试卷“上半场”的应用题今年移到了第18题（倒数第三题），难度也相应有所增加。</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> </p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"> 范书韵表示，今年总体难度应该说在考生心理预期的范围之内。在今年的《考试说明》中就曾明确指出，“有必要区分度和难度”，因此在复习及模拟考试中，老师和考生都做了一定准备。“总体而言，这是一份不错的试卷，整体结构平稳，设置一定区分度也有利于高校人才的选拔。”</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"><img src=\"https://i01piccdn.sogoucdn.com/696e03f89ced96b4?.png\" style=\"box-sizing: border-box; border-style: none; vertical-align: top; height: auto; -webkit-text-stroke: 0.05px !important;\"></p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"></p>\r\n<h3 style=\"box-sizing: border-box; font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" line-height:=\"\" 1.2;=\"\" color:=\"\" rgb(38,=\"\" 38,=\"\" 38);=\"\" margin-top:=\"\" 30px;=\"\" font-size:=\"\" 18px;=\"\" -webkit-text-stroke-width:=\"\" 0.05px;=\"\" padding-left:=\"\" 10px;=\"\" border-left:=\"\" 4px=\"\" solid=\"\" rgb(0,=\"\" 136,=\"\" 204);=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"></h3>\r\n<span style=\"color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" -webkit-text-stroke-width:=\"\" 0.05px;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">难。</span>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"></p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">很多考生都抱怨说2021年的数学试题太难，看不懂题目，让人抓不着头绪，而且据考生反馈，2021年数学的“压轴题”是特别难的。专业老师在线权威答疑 zy.offercoming.com</p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"><img src=\"https://i01piccdn.sogoucdn.com/22e540018aa73038?.png\" style=\"box-sizing: border-box; border-style: none; vertical-align: top; height: auto; -webkit-text-stroke: 0.05px !important;\"></p>\r\n<p style=\"box-sizing: border-box; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\"></p>\r\n<p style=\"box-sizing: border-box; margin-bottom: 20px; -webkit-text-stroke-width: 0.05px; line-height: 1.8; overflow-wrap: break-word; white-space: pre-wrap; color: rgb(38, 38, 38); font-family: \" helvetica=\"\" neue\",=\"\" helvetica,=\"\" \"pingfang=\"\" sc\",=\"\" \"hiragino=\"\" sans=\"\" gb\",=\"\" \"microsoft=\"\" yahei\",=\"\" 微软雅黑,=\"\" arial,=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" -webkit-text-stroke-color:=\"\" initial=\"\" !important;\"=\"\">以上就是关于江苏高考数学难度全部的内容，如果想了解江苏高考数学难度更多相关内容，可以关注我们，你们的支持是我们更新的动力！</p>', 1, '', '今年,江苏,江苏高考,高考数学', '今年江苏高考数学难吗\r\n2022江苏高考数学还是比较难的，考的内容非常基础，但是题目创新性非常高，这给很多考生带来了不小的压力。\r\n高考试卷难度单单从试卷的试题本身来说，这...', '原创', 0, '小编', 1, 1, 1676452100, 1676452100, 1676966095, 0);

-- ----------------------------
-- Table structure for hui_upload_file
-- ----------------------------
DROP TABLE IF EXISTS `hui_upload_file`;
CREATE TABLE `hui_upload_file`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `storage` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '存储方式',
  `group_id` int(11) NOT NULL DEFAULT 0 COMMENT '文件分组id',
  `file_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '存储域名',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件路径',
  `file_size` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小(字节)',
  `file_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件类型',
  `extension` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件扩展名',
  `is_image` int(1) NOT NULL DEFAULT 1,
  `is_delete` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '软删除',
  `upload_ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `user_id` int(10) NOT NULL DEFAULT 0 COMMENT '上传人员id',
  `role_id` int(10) NOT NULL DEFAULT 0 COMMENT '角色id，如果有值为后端，无值为前端会员',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for hui_upload_group
-- ----------------------------
DROP TABLE IF EXISTS `hui_upload_group`;
CREATE TABLE `hui_upload_group`  (
  `group_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `group_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件类型',
  `group_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类排序(数字越小越靠前)',
  `wxapp_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '小程序id',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`group_id`) USING BTREE,
  INDEX `type_index`(`group_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of hui_upload_group
-- ----------------------------
INSERT INTO `hui_upload_group` VALUES (1, 'image', '1', 0, 0, 1678759440, 1678759440);

SET FOREIGN_KEY_CHECKS = 1;
